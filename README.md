# Blocks for Obsidian

Adds Notion-like blocks to the editor for easy content rearangement

## Example

![Notion user dragging a block of text to reorder it with the other blocks of text.](https://i.stack.imgur.com/Ggw4d.gif)
