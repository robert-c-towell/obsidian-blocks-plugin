import {
	App,
	Plugin,
	PluginSettingTab,
	Setting,
	addIcon,
	setIcon,
} from "obsidian";
import { RangeSetBuilder } from "@codemirror/rangeset";
import {
	EditorView,
	Decoration,
	DecorationSet,
	ViewUpdate,
	WidgetType,
	ViewPlugin,
} from "@codemirror/view";

/**
 * Figure out how to create an element on the screen that is a copy of the textline you want when you drag it
 * Learn about statefields so you can effectively cut/paste the line
 */

interface BlocksSettings {
	mySetting: string;
}

const DEFAULT_SETTINGS: BlocksSettings = {
	mySetting: "default",
};

export default class BlocksForObsidian extends Plugin {
	settings: BlocksSettings;

	async onload() {
		console.log("loading plugin.");
		await this.loadSettings();

		addIcon(
			"bfo-handle",
			`<g class="layer">
        <path d="m70,70a10,10 0 1 0 0,20a10,10 0 1 0 0,-20zm0,-28.57a10,10 0 1 0 0,20a10,10 0 1 0 0,-20zm10,-18.57a10,10 0 1 0 -20,0a10,10 0 1 0 20,0z" id="svg_2"/>
        <path d="m31,70a10,10 0 1 0 0,20a10,10 0 1 0 0,-20zm0,-28.57a10,10 0 1 0 0,20a10,10 0 1 0 0,-20zm10,-18.57a10,10 0 1 0 -20,0a10,10 0 1 0 20,0z" id="svg_3"/>
       </g>`
		);

		this.addSettingTab(new BlocksSettingTab(this.app, this));

		const ext = this.buildBlocksExtension();
		this.registerEditorExtension(ext);
	}

	onunload() {
		console.log("unloading plugin.");
	}

	async loadSettings() {
		this.settings = Object.assign(
			{},
			DEFAULT_SETTINGS,
			await this.loadData()
		);
	}

	async saveSettings() {
		await this.saveData(this.settings);
	}

	buildBlocksExtension() {
		const viewPlugin = ViewPlugin.fromClass(
			class {
				decorations: DecorationSet;

				constructor(view: EditorView) {
					this.decorations = this.buildDecorations(view);
				}

				update(update: ViewUpdate) {
					if (update.docChanged || update.viewportChanged) {
						this.decorations = this.buildDecorations(update.view);
					}
				}

				destroy() {}

				buildDecorations(view: EditorView) {
					class BlockHandleWidget extends WidgetType {
						constructor() {
							super();
						}

						eq(other: BlockHandleWidget) {
							// return other.isFolded == this.isFolded;
							return true;
						}

						toDOM() {
							let el = document.createElement("div");
							el.className = "bfo-handle";
							setIcon(el, "bfo-handle");
							return el;
						}

						ignoreEvent() {
							return false;
						}
					}

					let builder = new RangeSetBuilder<Decoration>();
					for (let { from, to } of view.visibleRanges) {
						// Iterate over the visible text and add a block handle
						// Need to incremement by length of line because the RangeSetBuilder works on character number, not line number
						// TODO: Consider grouping lines between linebreaks
						let currentCharacterNumber = from;
						const textIter = view.state.doc.iterRange(from, to);
						while (!textIter.done) {
							if (
								!textIter.lineBreak &&
								textIter.value.trim() !== ""
							) {
								let decoration = Decoration.widget({
									widget: new BlockHandleWidget(),
									side: -1,
									inlineOrder: true,
								});
								builder.add(
									currentCharacterNumber,
									currentCharacterNumber,
									decoration
								);
							}

							currentCharacterNumber += textIter.value.length;
							textIter.next();
						}
					}
					return builder.finish();
				}
			},
			{
				decorations: (v) => v.decorations,
				eventHandlers: {
					mousedown: (e, view) => {
						let target = (e.target as HTMLElement).closest(
							".bfo-handle"
						);
						if (target) {
							console.log("mousedown");
						}
					},
				},
			}
		);

		return viewPlugin;
	}
}

class BlocksSettingTab extends PluginSettingTab {
	plugin: BlocksForObsidian;

	constructor(app: App, plugin: BlocksForObsidian) {
		super(app, plugin);
		this.plugin = plugin;
	}

	display(): void {
		const { containerEl } = this;

		containerEl.empty();

		new Setting(containerEl)
			.setName("Setting #1")
			.setDesc("It's a secret")
			.addText((text) =>
				text
					.setPlaceholder("Enter your secret")
					.setValue(this.plugin.settings.mySetting)
					.onChange(async (value) => {
						this.plugin.settings.mySetting = value;
						await this.plugin.saveSettings();
					})
			);
	}
}
